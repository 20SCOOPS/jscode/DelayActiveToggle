# DelayActiveToggle : class
- activate something with delay
- can set minimum activate duration
- use with something like toggle **Display Loading**

## Constructor
1. activeHandler : function()
2. deactiveHandler : function()
3. delay : Interval
    - default 0.5 sec
4. minimumDuration : Interval
    - default 1.0 sec

## Variable

1. activeHandler : function()
    - do after *requestActivate*
2. deactiveHandler : function
    - set from construct 
    - do after *requestDeactivate*
3. delay : Interval
    - delay before *activeHandler* revoke after call *requestActivate*
4. minimumDuration : Interval
    - mininum duration between *activeHandler* and *deactiveHandler* are called

## Method

1. requestActivate()
    - call when want to do *activeHandler*
2. requestDeactivate()
    - call when want to do *deactiveHandler*