export default class DelayActiveToggle {
    constructor(
        activeHandler,
        deactiveHandler,
        delay = 500,
        minimumDuration = 1000
    ){
        this.activeHandler = activeHandler,
        this.deactiveHandler = deactiveHandler,
        this.delay = delay,
        this.minimumDuration = minimumDuration

        // default task is 0 mean no task run
        this.deactiveTask = 0
        this.activeTask = 0
    }
    requestActivate() {
        //clear deactiveTask before activeTask
        clearTimeout(this.deactiveTask)
        this.deactiveTask = 0
        // should not duplicate activeTask  
        if (this.activeTask === 0) {
            //set activeAt for check duration of activated
            this.activeAt = Date.now() + this.delay
            this.activeTask = setTimeout(this.activeHandler,this.delay)
        }
    }
    requestDeactivate() {
        //clear activeTask before deactiveTask
        clearTimeout(this.activeTask)
        this.activeTask = 0
        // should not duplicate deactiveTask  
        if (this.deactiveTask === 0) {
            // find leftDuration from minimumDuration - duration
            let leftDuration = this.minimumDuration - (Date.now() - this.activeAt)
            // if activated duration more than minimumDuration then activated it
            // else activated after leftDuration
            if (leftDuration <= 0)
                this.deactiveHandler()
            else 
                this.deactiveTask = setTimeout(this.deactiveHandler,leftDuration)
        }
    }
}
module.exports = DelayActiveToggle